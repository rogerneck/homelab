# Argocd

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

// patch to expose argocd UI to node port
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "NodePort"}}'
// get dashboard default secret
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

## Kind

NodePort + NodeIP (`kubectl get nodes -o wide`)
Or temporally: `kubectl port-forward -n argocd service/argocd-server 8443:443`

### Force app delete

`kubectl patch application elasticelk --type=json -p='[{"op": "remove", "path": "/metadata/finalizers"}]' -n argocd`

# Ingress

## Nginx-ingress

Not needed as K3s comes with Trafik as ingress service


# Secrets

```bash
echo -n "<your-token-value>" | base64
```
